const { withPlausibleProxy } = require('next-plausible')
const { createVanillaExtractPlugin } = require('@vanilla-extract/next-plugin')
const withVanillaExtract = createVanillaExtractPlugin()

/** @type {import('next').NextConfig} */
const nextConfig = {
  webpack: (config) => {
    return config
  },
}

module.exports = withVanillaExtract(nextConfig)
