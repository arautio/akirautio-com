const fs = require('fs')
const prettier = require('prettier')
const blogs = require('../src/blogs/blogs.json')

const paths = blogs.map((blog) => `/blog/${blog.id}`)

const result = `
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
			<url>
				<loc>https://akirautio.com</loc>
      </url>
      <url>
				<loc>https://akirautio.com/blog</loc>
			</url>
			${paths
        .map((blog) => `<url><loc>https://akirautio.com${blog}</loc></url>`)
        .join('')}
		</urlset>
		`
console.log(result)
fs.writeFileSync(
  `${__dirname}/../public/sitemap.xml`,
  prettier.format(result, {
    semi: false,
    parser: 'html',
    embeddedLanguageFormatting: 'off',
  })
)
