const puppeteer = require('puppeteer')
const fs = require('fs')

const takeScreenshot = async function (name, html) {
  const browser = await puppeteer.launch({})
  const page = await browser.newPage()
  await page.setViewport({ width: 800, height: 420 })
  await page.setContent(html, { waitUntil: 'networkidle0' })
  await page.screenshot({ path: '../public/assets/social/' + name + '.png' })
  await browser.close()
}

const readTemplate = async function (title) {
  const data = fs.readFileSync('../socialMediaTemplate/index.html', 'utf8')
  return data.replace('[TITLE]', title)
}

const createImage = async function (name, title) {
  const html = await readTemplate(title)
  const file = await takeScreenshot(name, html)
}

const blogs = require('../src/blogs/blogs.json')
blogs.map((blog) => {
  createImage(blog.id, blog.name)
})
