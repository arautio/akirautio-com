Starting to create a common component library with a ready-made component library bring the most value in the beginning. These component libraries usually include some set of frequently used elements on the web and also give a chance for theming to speed up developing.

There is a long history of ready-made component libraries that starts even before React. One of the most popular ones has been [Bootstrap](https://getbootstrap.com/) for a very long time. Due to more complex design setups and better tools, a lot of ready-made component libraries for React have popping up in past years. Many of them are developed or at least initiated by big tech companies who also use them internally.

# Reasoning to use component libraries

The biggest advantage of using a ready-made component library is that a lot of components have been already developed and tested before the project even starts to develop. This gives a boost at the beginning of the project since no time spend on writing the basic components like buttons and typography elements. That time can be used for feature development instead.

Most if not all ready-made component libraries also provide theming, so that design of the components can be aligned with the project in hand.

Though there are two risks using a ready-made component library. Firstly the project will be very dependent on the library so any issues or limitations in components from library will take time to get sorted out.

The ready-made component library also dictates pretty much which kind of tools should be used for developing the design. For example, Material UI uses the CSS-in-JS through their internal styled function. Using styled-components or pure CSS will cause issues and unnecessary complexity to the project.

# Available ready made component libraries

Probably one of the most known ready-made component library nowadays is Material UI which is based on Google material design. There are also tons of other options available, though many of them are still pretty young.

I would start by looking following list first:

- [Material UI](https://material-ui.com/)
- [BaseWeb](https://baseweb.design/)
- [Elastic UI](https://elastic.github.io/eui/#/)
- [Reakit](https://reakit.io/)
- [Chakra-UI](https://chakra-ui.com/)

All mentioned packages are currently in active development so new features and fixes are coming constantly. While Material UI is the most popular one, all of them provide something unique.

BaseWeb uses Styletron and makes override the base style while Elastic UI enables to use both scss and styled-component and offers a big variety of components. Reakit focuses on keeping a bundle size very small and ChakraUI has all components accessible.

# How to select the one for me

Since a component library will dictate a big part of the application, it's important to make a decision that fits to the project. Each project should use their parameters to define what fits but a good start would be to ensure that tools align, compatibility is enough for project needs and there are needed a component for the project.

For the project, it would help a lot if the component library would use the same tools as the project itself (like styled-components) or that the tools the library uses, align with the idea what the project has. For example, BaseWeb uses styletron which is pretty close to styled-components but using styled-components and styletron in same project doesn't make sense.

Another risk point is compatibility. Even a well made component library may cause a lot of issues if it's not aligned with other packages in projects. The biggest risks usually depend on the project but forms are usually the ones that cause issues. If component don't pass the actions or ref through, it may limit the usage of certain packages.

Even a well build component library isn't usable if it's lacking many of the needed components in a project. Unfortunately packages group and name the components a bit differently so it takes quite some time to go through the list. It should also be taken into account that if a package has quite low-level (like popper and transition) components, there is less need to be fully fitting since those components should a lot easier to compose together from low level components.

# My experience

I have been using Material UI, BaseWeb and Chakra-UI. In my opinion, all of them work very well in their scope. If I would focus on creating a web application mostly to mobile, I would probably use Material UI due to strong similarities to Android design language.

For smaller projects, I have used BaseWeb and ChakraUI due to easy extendability and CSS-in-JS support. While both felt easy to use and simple to modify to my design, I liked more ChakraUI since it used styled-components.

# Summary

Component libraries have a long history from the time before React. One of the most popular ones has been Bootstrap but nowadays there are a lot options that work better in React.

These libraries usually bring a good amount of ready-made components which will ease up the development in the beginning and set a base for a common component library.

The most popular ready-made component package is Material UI but there are a lot of smaller ones that may give a better result for your specific project.

The importance is to create the criteria for your project and select the one that fits the best. The criteria can include underlying tools in package, extendability and component amount in relation to what's needed.
