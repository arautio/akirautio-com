# Tools to enhance your refining skills

## Tools for the good refinement

Well refined story isn't about defining what code needs to be written and where. It's about understanding the steps needed to be able finish the feature.

If the feature can be developed with already defined patterns in an application, then all is needed to define the steps and estimate their amounts.

On the other hand, if the feature needs something unknown, you need indepth ways to be able to eventually estimate the task. I have seen following methods very usable.

### Balance effort to value

First thing to define when doing something unknown, is that what are the alternatives. Can this be done differently which would lead to more known solution? Is there enough value coming from the feature compared to it's effort to make it.

### Generalize problem

What is the problem you are trying to define on high level?

### Draw it

In a case the feature is complex, it may be better to draw a high level diagram about it before starting the development.

### Proof of Concepts

If the technology or concept behind the feature is new to you, it might be worth to even have a short Proof of concept from it before starting the development. Narrowing down the focus to one part instead of trying to fiddle it inside the bigger application, may give you better insights what needs to be done.

## Amount refinement connected to risk appetite

## Results

- TDD
- Less bugs and misalignemnt
- Better estimations
-

## Summary
