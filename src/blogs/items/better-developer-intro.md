Software development isn't just writing good code. It's also about whether the right features are being delivered at the right time.

This also means that the best developer is not the one who can write the best code. Instead, it's a combination of various abilities that allows the team to produce the right functionality at the right time.

# Software development is a process

The code is newer written in isolation. It's one step in a development process delivering new features (or fixing issues). The process depends on the organization and type of project but the following three steps are always present:

1. A feature refinement
2. Development
3. Maintenance

The features have to be defined before any code can be written. After the feature has been developed, it needs to be delivered and maintained.
While the developer's focus is on the development step, a good developer doesn't just know how to write a good code, but he also acknowledges the other steps.In my experience, this also means that a good developer does not necessarily need to be an outstanding programmer.

The specific skills depend on the specific position, the team and the stakeholders. For example, the ability to grasp business logic and suggest alternatives allows the developer to optimize the development towards greater value within the product team. In the best case, the same benefit can be accomplished without any code, which also reduces maintenance.

# Process steps

Splitting the development process into smaller parts helps to better understand what happens when the new features are being developed. The steps we use in this article are not the only way to separate the process, and they have been chosen because they help to demonstrate the useful skills.

The software development process I introduced earlier is for a single feature, but the same thing can be applied to a high level as the same cycle is repeated in each feature.

![Development process](/assets/blog/better-developer/process-chart.svg)

If software development is applied to a large project, we will need a technical development step to ensure that our process does not slow down due to the amount of technical debt that we have in our project.

![Development process](/assets/blog/better-developer/steps.svg)

In following four chapters I'm introducing the main areas how the good developer can bring value on high level. Each of these points will get their own in-depth articles later on.

## 1. Feature refinement

The aim of the feature refinement is to identify the required changes/additions to the current application and to break the feature into smaller pieces in order to be able to test and release it incrementally. Although responsibility for this process rests with a person who understands the needs of the customer, the best outcome is when the whole team is involved as soon as possible.

As a developer, I have seen two key areas to influence the result of refinements; **affecting how the features are build** and **how long they take to develop**.

Being part of the feature refinement, a good developer can influence the result of the feature to be technically more feasible and it brings the value that is defined for the feature. For example instead of developing a multistep form to request information from the user, maybe the information is available already somewhere and it can be automatically fetched.

Estimation is another important aspect of refinement, since it helps to understand how long those features will take to get to the customer. The developer has a key role to play here, as the better the prediction is made, the easier it is to set expectations for the user. Therefore a good developer should have the ability to provide a high-level estimate of how long tasks take and the ability to communicate and cope with the uncertainties of the tasks.

At the end of the estimation process, it should be reasonably clear how long the development of the feature will take and what needs to be done.

## 2. Development

The creation step is not just the writing of a code. It's also about having a common view about development in a team and sharing the knowledge while creating new features. This can be only done if the code and patterns are understandable to everyone and it's done with good programming practices. Therefore I'd like to raise two primary areas; high-quality discussions about development within the team and a tested and clean code.

There are plenty of ways to achieve a good conversation atmosphere inside the team and many more ways to get these conversations going at the right time. Ultimately, it comes to the willingness of individuals to interact and negotiate with each other. Having these skills is one of the most significant attributes of a good developer. Everyone does these things a little differently, but the most important part is to know your strengths and weaknesses and to find a way to improve these skills.

Based on my experience, one of the early signs of a good developer is that their code is written in a testable manner and that proper unit and integration tests are carried out as part of the creation of the feature. This does not necessarily mean that the code coverage is 100%, but to ensure that critical factors have been tested and that uses cases of the code are documented.

## 3. Maintenance

Maintenance in this context means all those activities that happen when the code goes to production and works as intended. While there are a lot of project related areas to take into account, every project has following two:

The first is the release process and the infrastructure. A good developer has at least an understanding of the release process since it helps to ensure that changes in the code do not break the release.

Error management is another key area. Even the best code doesn't always work as planned, so it's important to be able to recognize the cause of the problem. As a good developer, you can bring value by understanding how to set up a proper logging system and how to use it to debug problems or bottlenecks.

## 4. Technical development

Not every feature has direct benefits for the user. Technical debt might get too big to keep the feature development fast enough or the new features need significant changes original setup. To my experience, a good developer can both justify these changes to the team so they get prioritized accordingly and a good developer can see when those changes are needed ahead of time so they don't come as a surprise.

# Further reading

In this article, we've just scratched the surface of these topics, so in the future, I'm going to concentrate on every single issue.
Although many of these reflections have deviated from personal experience, my journey has been inspired a lot by Robert Martin's book [Clean Agile](https://www.oreilly.com/library/view/clean-agile-back/9780135782002/).

If you have any comments or questions, I would gladly hear them.
