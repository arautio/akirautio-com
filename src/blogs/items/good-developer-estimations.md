# Intro

Estimating time it takes to develop a feature is hard and also very beneficial.

# Meaning of estimation and conflict between developer and project management

- From project management perspective planning is the key. House building example?
- But developers perspective the work isn't just execution, it's about refinement and research.

# Estimation is not just hours

- Personal experience
- Reference from another book
  [Habit 11: Don’t Underestimate from /home/aki/Documents/Books/14-habits-of-highly-productive-developers.epub]

- Estimation is a scale

- Storypoints to rescue

# Slicing the feature into chunks

# Controlling uncertainty of the estimation

The development work can be split into two pieces, the programming tasks and research / refinement tasks. From these two the programming tasks are easier to estimate while research and refinment is the one that brings uncertainty.

# Finding a balance in estimation

- What I know and what I don't know
  --> The parts you know are easier to estimate than the parts you don't.

- Do I need to look more to get better idea
  --> Refining the story more could give a better idea
