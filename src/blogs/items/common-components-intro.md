A common component library/design library is a set of visual components that builds up a base for the application. The library usually includes _Typography elements_ like headings and paragraphs, _form elements_ like buttons and input fields, and _page elements_ like tabs and accordions. These can be implemented inside the project or as a separate project.

## Definition

While a design library often refers only to the visual elements, a common component library can also have components that help out building screens. These may be stack or translation components, for instance. In this series, we speak about a popular library of components, but in most instances, the content also applies to design libraries.

The scope of the common component system varies depending on the needs, and although some components are included more often than others, each system is unique.

## Benefits

The main driver to build a common component library is to have a consistent design language between different parts of the application (or between applications) and speed up the development. The key to a successful software project is to strike a balance between software quality and a pace of development, and both sides will benefit from a properly build component library.

With a common library, there is a possibility to create a reusable set of elements that are well tested. The time used in the beginning to develop the set takes roughly the same time as developing normal components but once they are done, the reuse starts to cut the development speed tremendously.

For example, form components can have an error handling and loading states included so that they are available for all kinds of components and work the same tested way all the time.

## Options

When starting up with a common component library, there are three different ways to go:

1. Use SASS and css classname-helpers
2. Use CSS-in-JS like styled-components or emotion
3. Use a ready-made component library like Material-UI or base-web

The correct choice depends on a state of the project because you don't want to rewrite the current component too much. For the new project, all options are good while the existing project should select one which fits the current setup.

Using a ready-made component library like Material-UI speeds things up hugely in the beginning because there are already a huge amount of commonly used components ready and they only need styling. With two other options, the beginning will be slightly slower because most of the components need to be developed from the beginning. This may end up being beneficial later because ready-made component libraries may lack some needed features which may take more time to develop due to the way the library works.

There are many good design systems already existing and they can give inspiration for the start. [Adele](https://adele.uxpin.com/) lists many popular ones and there are a lot more available online.

## Summary

The key benefit of creating a common component library is to have a set of clearly defined and tested components that can be reused over and over again. This will speed up the development and ensure, the design stays consistent and the code is tested and working. There are a huge amount of different options for React to help out the creation of common components and the right choice depends on the state and needs of the project.

_This is a first post from the series **Speed up development by creating a common component library**. The later posts will cover different options to build the common component library and how to document the library._
