import React from 'react'
import { motion } from 'framer-motion'
import * as styles from './Sun.css'

const Sun = () => (
  <div className={styles.sunWrapper}>
    <motion.div
      initial={{ y: -150 }}
      animate={{ y: 0 }}
      transition={{
        delay: 2,
        duration: 0.1,
        type: 'spring',
        stiffness: 100,
      }}
    >
      <div className={styles.sunLogo} />
    </motion.div>
  </div>
)

export default Sun
