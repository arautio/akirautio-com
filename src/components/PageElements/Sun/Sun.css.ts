import { style } from '@vanilla-extract/css'
import { vars } from '@/config/theme.css'

export const sunLogo = style({
  backgroundColor: '#FDB813',
  width: '120px',
  height: '120px',
  borderRadius: '60px',
})

export const sunWrapper = style({
  position: 'absolute',
  left: '10%',
  top: '3%',
})
