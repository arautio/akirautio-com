import { style, keyframes, globalStyle } from '@vanilla-extract/css'

const moveWave = keyframes({
  '0%': { transform: 'translate3d(-90px, 0, 0)' },
  '100%': { transform: 'translate3d(85px, 0, 0)' },
})

export const positionWrapper = style({
  position: 'absolute',
  bottom: 0,
  width: '100%',
  height: '95px',
  overflow: 'hidden',
})

export const wave = style({
  position: 'relative',
  width: '100%',
  height: '15vh',
  marginBottom: '-7px',
  /*Fix for safari gap*/
  minHeight: '100px',
  maxHeight: '150px',
  zIndex: '3',
})

globalStyle(`${wave} > g > use `, {
  animation: `${moveWave} 40s cubic-bezier(0.55, 0.5, 0.45, 0.5) infinite`,
})

globalStyle(`${wave} > g > use:nth-child(1)`, {
  animationDelay: '-2s',
  animationDuration: '7s',
})

globalStyle(`${wave} > g > use:nth-child(2)`, {
  animationDelay: '-3s',
  animationDuration: '10s',
})

globalStyle(`${wave} > g > use:nth-child(4)`, {
  animationDelay: '-5s',
  animationDuration: '20s',
})
