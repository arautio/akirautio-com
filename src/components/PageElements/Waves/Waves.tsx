import React from 'react'
import { motion } from 'framer-motion'
import * as styles from './Wave.css'

const Waves = () => (
  <div className={styles.positionWrapper}>
    <motion.div
      initial={{ y: 150 }}
      animate={{ y: 0 }}
      transition={{
        delay: 2,
        duration: 0.1,
        type: 'spring',
        stiffness: 100,
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 24 150 28"
        preserveAspectRatio="none"
        shapeRendering="auto"
        className={styles.wave}
      >
        <defs>
          <path
            id="gentle-wave"
            d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"
          />
        </defs>
        <g>
          <use xlinkHref="#gentle-wave" x="48" y="0" fill="rgba(0, 27, 72,1)" />
          <use
            xlinkHref="#gentle-wave"
            x="48"
            y="3"
            fill="rgba(1,138,189,0.8)"
          />
          <use
            xlinkHref="#gentle-wave"
            x="48"
            y="5"
            fill="rgba(151, 203, 220,0.5)"
          />
          <use xlinkHref="#gentle-wave" x="48" y="7" fill="#0099ff" />
        </g>
      </svg>
    </motion.div>
  </div>
)

export default Waves
