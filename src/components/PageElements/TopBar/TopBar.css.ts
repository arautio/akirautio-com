import { style } from '@vanilla-extract/css'
import { vars } from '@/config/theme.css'

export const wrapper = style({
  position: 'fixed',
  borderBottom: '1px solid',
  borderColor: vars.colors.codegray,
  width: '100%',
  backgroundColor: 'white',
  margin: '0',
  height: '50px',
  display: 'grid',
  gridTemplateColumns: '1fr repeat(1, auto) 1fr',
  alignItems: 'center',
  justifyItems: 'center',
})
