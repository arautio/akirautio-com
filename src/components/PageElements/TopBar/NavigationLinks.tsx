import React from 'react'
import Hamburger from 'hamburger-react'
import * as styles from './NavigationLinks.css'

interface NavigationLinksProps {
  children: any
}

const NavigationLinks: React.FC<NavigationLinksProps> = ({ children }) => {
  const [isToggled, setToggle] = React.useState(false)
  return (
    <div className={styles.wrapper}>
      <div className={styles.mini}>
        <Hamburger
          toggled={isToggled}
          toggle={setToggle}
          label="Show navigation menu"
          hideOutline={false}
          size={24}
        />
      </div>
      <div className={styles.maxiMenu}>{children}</div>
      {isToggled ? <div className={styles.miniMenu}>{children}</div> : null}
    </div>
  )
}

export default NavigationLinks
