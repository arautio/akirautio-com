import { useViewportScroll, motion, useTransform } from 'framer-motion'
import React, { useEffect } from 'react'
import * as styles from './HidingTitle.css'

interface HidingTitleProps {
  children: any
}

const calculateScrolledInPercentage = () => {
  if (typeof document !== 'undefined') {
    const windowScroll =
      document.body.scrollTop || document.documentElement.scrollTop
    const height =
      document.documentElement.scrollHeight -
      document.documentElement.clientHeight
    return windowScroll
  }
  return 0
}

const tranformation = (value: number, min: number = 38, max: number = 100) => {
  if (value <= min) {
    return 50
  } else if (value > max) {
    return 0
  }
  const startValue = (value - min) / (max - min)
  return 50 - startValue * 50
}

const HidingTitle: React.FC<HidingTitleProps> = ({ children }) => {
  const [start, setStart] = React.useState(true)
  const { scrollY } = useViewportScroll()
  scrollY.set(calculateScrolledInPercentage())
  const top = useTransform(scrollY, (value: number) =>
    tranformation(value, 40, 140)
  )
  const visibility = useTransform(scrollY, (value: number) =>
    value <= 40 ? 'hidden' : 'visible'
  )
  useEffect(() => {
    setStart(false) // Force refresh the top bar once.
  }, [])
  return (
    <div className={styles.wrapper}>
      <motion.div
        style={{ position: 'relative', top, visibility }}
        suppressHydrationWarning={true}
      >
        {start ? null : children}
      </motion.div>
    </div>
  )
}

export default HidingTitle
