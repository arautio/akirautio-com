import React from 'react'
import { useTransform, useViewportScroll, motion } from 'framer-motion'
import * as styles from './TopBar.css'

import ScrollIndicator from '../ScrollIndicator/ScrollIndicator'

interface TopBarProps {
  children: any
}

const TopBar: React.FC<TopBarProps> = ({ children }) => {
  const { scrollY } = useViewportScroll()
  let bottom = scrollY.get() + 50
  const top = useTransform(scrollY, (value: number) => {
    if (value > bottom) {
      bottom = value
    } else if (value < bottom - 50) {
      bottom = value + 50
    }
    return -51 - (value - bottom)
  })
  const indicator = useTransform(top, (value: number) => value + 50)
  return (
    <React.Fragment>
      <motion.header
        className={styles.wrapper}
        style={{
          top,
        }}
      >
        {children}
      </motion.header>
      <ScrollIndicator top={indicator} />
    </React.Fragment>
  )
}

export default TopBar
