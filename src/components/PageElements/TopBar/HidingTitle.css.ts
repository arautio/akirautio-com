import { style } from '@vanilla-extract/css'

export const wrapper = style({
  overflow: 'hidden',
  height: '48px',
  gridColumnStart: '2',
})
