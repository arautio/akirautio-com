import { style } from '@vanilla-extract/css'
import { vars } from '@/config/theme.css'

export const wrapper = style({
  marginLeft: 'auto',
})

export const mini = style({
  '@media': {
    'screen and (min-width: 40em)': {
      display: 'none',
    },
  },
})

export const miniMenu = style({
  position: 'absolute',
  top: '51px',
  right: '1px',
  backgroundColor: 'white',
  padding: '12px 0',
  display: 'flex',
  flexDirection: 'column',
  gap: '21px',
  borderStyle: 'solid',
  borderColor: vars.colors.codegray,
  borderWidth: '0 1px 1px 1px',
  '@media': {
    'screen and (min-width: 40em)': {
      display: 'none',
    },
  },
})

export const maxiMenu = style({
  '@media': {
    'screen and (max-width: 40em)': {
      display: 'none',
    },
  },
})
