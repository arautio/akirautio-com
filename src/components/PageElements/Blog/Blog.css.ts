import { style } from '@vanilla-extract/css'
import { vars } from '@/config/theme.css'

export const centerWidth = style({
  maxWidth: '65ch',
  margin: '0 auto',
})

export const centerWidthMargin = style({
  maxWidth: '800px',
  margin: '20px auto',
})

export const header = style({
  margin: '60px 0',
})

export const content = style({
  padding: '0 24px',
})

export const footer = style({
  padding: '0 24px',
  margin: '42px 0',
})

export const image = style({
  width: '100%',
})

export const codeBackground = style({
  height: '250px',
  backgroundColor: vars.colors.codegray,
  borderRadius: '6px',
})

export const blockQuote = style({
  borderLeft: '2px solid',
  paddingLeft: vars.space[4],
  borderColor: vars.colors.lightgray,
  paddingTop: '3px',
  paddingBottom: '3px',
})

export const footerLine = style({
  width: '20%',
  margin: '0 auto',
  height: '0px',
  borderTop: 'thin solid',
  borderColor: vars.colors.gray,
})

export const footerLinks = style({
  display: 'flex',
  justifyContent: 'center',
  paddingTop: vars.space[2],
  paddingBottom: vars.space[2],
})

export const footerCopy = style({
  textAlign: 'center',
  display: 'block',
  fontFamily: vars.fonts.logo,
  paddingTop: vars.space[2],
  paddingBottom: vars.space[2],
})

export const list = style({})

export const listItem = style({
  lineHeight: '1.2em',
  marginTop: vars.space[5],
  marginBottom: vars.space[5],
})

export const titleList = style({
  display: 'flex',
  alignItems: 'flex-start',
  marginBottom: vars.space[1],
})

export const dateParagraph = style({
  fontFamily: vars.fonts.title,
  marginLeft: 'auto',
  lineHeight: '1.5em',
  fontSize: vars.fontSizes[1],
  '@media': {
    'screen and (max-width: 40em)': {
      fontSize: vars.fontSizes['08'],
    },
  },
})

export const titleItem = style({
  fontFamily: vars.fonts.title,
  lineHeight: '1.5em',
  fontSize: vars.fontSizes[1],
  margin: 0,
  fontWeight: 'normal',
  paddingRight: vars.space[2],
  '@media': {
    'screen and (max-width: 40em)': {
      fontSize: vars.fontSizes['08'],
    },
  },
})

export const contentTitle = style({
  marginBottom: '60px',
  '@media': {
    'screen and (max-width: 40em)': {
      marginBottom: '15px',
    },
  },
})
