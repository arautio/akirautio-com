import { style } from '@vanilla-extract/css'
import { vars } from '@/config/theme.css'

export const wrapper = style({
  position: 'sticky',
  overflow: 'hidden',
})

export const position = style({
  backgroundColor: vars.colors.gray,
  height: '4px',
  width: '10%',
})
