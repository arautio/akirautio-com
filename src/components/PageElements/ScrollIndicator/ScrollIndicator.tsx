import React from 'react'
import {
  useViewportScroll,
  motion,
  useTransform,
  MotionValue,
} from 'framer-motion'
import * as styles from './ScrollIndicator.css'

const calculateScrolledInPercentage = () => {
  if (typeof document !== 'undefined') {
    const windowScroll =
      document.body.scrollTop || document.documentElement.scrollTop
    const height =
      document.documentElement.scrollHeight -
      document.documentElement.clientHeight
    return windowScroll / height
  }
  return 0
}

interface ScrollIndicatorProps {
  top: MotionValue
}

const ScrollIndicator: React.FC<ScrollIndicatorProps> = ({ top = 0 }) => {
  const { scrollYProgress } = useViewportScroll()
  scrollYProgress.set(calculateScrolledInPercentage())
  const percentage = useTransform(
    scrollYProgress,
    (value: number) => `${value * 100}%`
  )

  return (
    <motion.div className={styles.wrapper} style={{ top }}>
      <motion.div
        className={styles.position}
        style={{ width: percentage }}
        suppressHydrationWarning={true}
      />
    </motion.div>
  )
}

export default ScrollIndicator
