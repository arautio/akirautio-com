import { style } from '@vanilla-extract/css'
import { vars } from '@/config/theme.css'

export const container = style({
  padding: vars.space[5],
})

export const listItem = style({
  padding: 0,
  marginTop: vars.space[2],
  marginBottom: vars.space[2],
  listStyleType: 'none',
})

export const list = style({
  margin: 0,
  padding: 0,
})
