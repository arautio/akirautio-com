import * as T from 'components/Typography'
import * as styles from './SimilarPosts.css'

interface Post {
  id: string
  title: string
}

interface SimilarPostsProps {
  similar: Array<Post>
}

export const SimilarPosts = ({ similar }: SimilarPostsProps) => {
  return similar !== undefined && similar.length > 0 ? (
    <div className={styles.container}>
      <hr />
      <T.ContentSubTitle>Related posts</T.ContentSubTitle>
      <ul className={styles.list}>
        {similar.map((post) => (
          <li className={styles.listItem} key={post.id}>
            <T.Link href={`/blog/${post.id}`}>{post.title}</T.Link>
          </li>
        ))}
      </ul>
    </div>
  ) : null
}
