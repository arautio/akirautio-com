import { style } from '@vanilla-extract/css'
import { vars } from '@/config/theme.css'

export const icon = style({
  width: '24px',
  height: '24px',
  paddingLeft: vars.space[2],
  paddingRight: vars.space[2],
  transition: '200ms linear',
})

export const iconLink = style({
  ':hover': {
    fill: vars.colors.blue,
  },
  textDecoration: 'none',
})
