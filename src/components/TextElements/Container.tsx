import React from 'react'
import * as styles from './Container.css'

const Container: React.FC = (props) => (
  <div className={styles.container}>{props.children}</div>
)

export default Container
