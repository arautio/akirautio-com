import { style } from '@vanilla-extract/css'

export const linkItem = style({
  display: 'inline',
})

export const links = style({
  zIndex: '999',
  width: '100%',
  textAlign: 'center',
  padding: '6px 0',
  margin: '0',
})
