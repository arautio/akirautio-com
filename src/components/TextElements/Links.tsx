import React from 'react'
import * as styles from './Link.css'

const Links: React.FC = (props) => (
  <ul className={styles.links}>{props.children}</ul>
)

export default Links
