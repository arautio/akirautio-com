import { style } from '@vanilla-extract/css'

export const container = style({
  height: '100%',
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  position: 'fixed',
  alignItems: 'center',
  justifyContent: 'center',
  zIndex: '4',
})
