import React from 'react'
import * as styles from './Link.css'

const LinkItem: React.FC = (props) => (
  <li className={styles.linkItem}>{props.children}</li>
)

export default LinkItem
