import { style, keyframes } from '@vanilla-extract/css'
import { vars } from '@/config/theme.css'

const fadeIn = keyframes({
  '0%': { opacity: 0 },
  '100%': { opacity: 1 },
})

export const header = style({
  fontFamily: vars.fonts.logo,
  fontSize: '5.5em',
  fontWeight: '900',
  color: '#353531',
  '@media': {
    'screen and (max-width: 40em)': {
      fontSize: '3.3em',
    },
  },
  '::selection': {
    backgroundColor: 'rgba(151, 203, 220,0.5)',
  },
  transition: 'all 0.2s',
  animation: `${fadeIn} 0.2s`,
  lineHeight: 1.25,
  margin: 0,
  padding: 0,
})

export const linkText = style({
  fontFamily: vars.fonts.title,
  fontSize: '1.5em',
  color: '#018abd',
  '@media': {
    'screen and (max-width: 40em)': {
      fontSize: '1em',
    },
  },
  '::selection': {
    backgroundColor: 'rgba(151, 203, 220,0.5)',
  },
  transition: 'all 0.2s',
  animation: `${fadeIn} 0.2s`,
  lineHeight: 1,
  margin: '0 8px',
  padding: 0,
})

export const logo = style({
  fontFamily: vars.fonts.logo,
  fontSize: vars.fontSizes[80],
  cursor: 'pointer',
  textAlign: 'center',
  fontWeight: 'bold',
  '@media': {
    'screen and (max-width: 40em)': {
      fontSize: vars.fontSizes[25],
    },
  },
  '::selection': {
    backgroundColor: 'rgba(151, 203, 220,0.5)',
  },
})

export const contentTitle = style({
  fontFamily: vars.fonts.title,
  fontSize: vars.fontSizes[3],
  lineHeight: '1.2em',
  fontWeight: vars.fontWeights[900],
  color: vars.colors.black,
  margin: 0,
  padding: 0,
  '@media': {
    'screen and (max-width: 40em)': {
      fontSize: vars.fontSizes[2],
    },
  },
})

export const contentDate = style({
  margin: 0,
  padding: 0,
  marginTop: vars.space[1],
  fontSize: vars.fontSizes['08'],
  fontFamily: vars.fonts.body,
  color: vars.colors.gray,
})

export const contentSubTitle = style({
  fontFamily: vars.fonts.title,
  fontSize: vars.fontSizes[2],
  fontWeight: 'bold',
  paddingTop: '1em',
  color: vars.colors.black,
  margin: 0,
  '@media': {
    'screen and (max-width: 40em)': {
      fontSize: vars.fontSizes[15],
    },
  },
})

export const contentMinorTitle = style({
  fontFamily: vars.fonts.title,
  fontSize: vars.fontSizes[15],
  fontWeight: 'bold',
  paddingTop: '1em',
  color: vars.colors.black,
  margin: 0,
  '@media': {
    'screen and (max-width: 40em)': {
      fontSize: vars.fontSizes[1],
    },
  },
})

export const paragraph = style({
  lineHeight: '1.778',
  fontFamily: vars.fonts.body,
  color: vars.colors.textGray,
  fontWeight: '400',
  fontKerning: 'normal',
  fontFeatureSettings: 'kern, liga, clig, calt',
})

export const strong = style({
  fontWeight: vars.fontWeights[900],
  color: vars.colors.black,
})

export const link = style({
  color: vars.colors.blue,
  fontFamily: vars.fonts.body,
})

export const listItem = style({
  paddingTop: vars.space[1],
  paddingBottom: vars.space[1],
  fontFamily: vars.fonts.body,
})

export const listParagraph = style({
  margin: 0,
  fontSize: vars.fontSizes['08'],
  color: vars.colors.gray,
})
