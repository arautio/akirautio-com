import React from 'react'
import * as styles from './Typography.css'

const Header: React.FC<React.HTMLProps<HTMLHeadingElement>> = (props) => (
  <h1 {...props} className={styles.header}>
    {props.children}
  </h1>
)

const LinkText: React.FC<React.HTMLProps<HTMLAnchorElement>> = (props) => (
  <a {...props} className={styles.linkText}>
    {props.children}
  </a>
)

const Logo: React.FC<React.HTMLProps<HTMLHeadingElement>> = (props) => (
  <h1 {...props} className={styles.logo}>
    {props.children}
  </h1>
)

const ContentTitle: React.FC<React.HTMLProps<HTMLHeadingElement>> = (props) => (
  <h2 {...props} className={styles.contentTitle}>
    {props.children}
  </h2>
)

const ContentDate: React.FC<React.HTMLProps<HTMLParagraphElement>> = (
  props
) => (
  <p {...props} className={styles.contentDate}>
    {props.children}
  </p>
)

const ContentSubTitle: React.FC<React.HTMLProps<HTMLHeadingElement>> = (
  props
) => (
  <h3 {...props} className={styles.contentSubTitle}>
    {props.children}
  </h3>
)

const ContentMinorTitle: React.FC<React.HTMLProps<HTMLHeadingElement>> = (
  props
) => (
  <h3 {...props} className={styles.contentMinorTitle}>
    {props.children}
  </h3>
)

const Paragraph: React.FC<React.HTMLProps<HTMLParagraphElement>> = (props) => (
  <p {...props} className={styles.paragraph}>
    {props.children}
  </p>
)

const Strong: React.FC<React.HTMLProps<HTMLSpanElement>> = (props) => (
  <strong {...props} className={styles.strong}>
    {props.children}
  </strong>
)

const Link: React.FC<React.HTMLProps<HTMLAnchorElement>> = (props) => (
  <a {...props} className={styles.link}>
    {props.children}
  </a>
)

const ListItem: React.FC<React.HTMLProps<HTMLLIElement>> = (props) => (
  <li {...props} className={styles.listItem}>
    {props.children}
  </li>
)

const ListParagraph: React.FC<React.HTMLProps<HTMLParagraphElement>> = (
  props
) => (
  <p {...props} className={styles.listParagraph}>
    {props.children}
  </p>
)

export {
  Header,
  Link,
  LinkText,
  Logo,
  ContentTitle,
  ContentDate,
  ContentSubTitle,
  ContentMinorTitle,
  Paragraph,
  Strong,
  ListItem,
  ListParagraph,
}
