export default {
  linkedin: 'https://www.linkedin.com/in/akirautio/',
  mastodon: 'https://masto.ai/@akirautio',
  devto: 'https://dev.to/akirautio',
  github: 'https://github.com/ARautio',
  blog: 'https://akirautio.com/blog',
}
