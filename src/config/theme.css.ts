import { createGlobalTheme, globalStyle } from '@vanilla-extract/css'

export const vars = createGlobalTheme(':root', {
  colors: {
    black: 'black',
    lightgray: 'rgb(180,180,180)',
    textGray: 'hsla(0,0%,0%,0.9)',
    gray: 'rgb(100, 100, 100)',
    blue: '#4361ee',
    codegray: 'rgb(240, 240, 240)',
  },
  space: {
    1: '5px',
    2: '10px',
    3: '15px',
    4: '20px',
    5: '25px',
  },
  fontSizes: {
    '08': '0.8em',
    15: '1.5em',
    1: '1.2em',
    2: '1.4em',
    3: '2.2em',
    25: '2.5em',
    80: '4em',
  },
  fontWeights: {
    900: '900',
  },
  fonts: {
    logo: 'Poppins, sans-serif',
    title: 'Poppins, sans-serif',
    body: 'Merriweather, sans-serif',
    code: 'Source Code Pro, Roboto, sans-serif',
    landingTitle: 'Poppins, sans-serif',
  },
})

globalStyle('body', {
  margin: '0',
  backgroundColor: 'white',
  fontSize: '18px',
  fontFamily: 'sans-serif',
})
