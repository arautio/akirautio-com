import React from 'react'
import Head from 'next/head'
import dynamic from 'next/dynamic'
import ReactMarkdown from 'react-markdown'
import Link from 'next/link'

import blogs from '../../blogs/blogs.json'

import * as T from 'components/Typography'
import Github from 'components/PageElements/Icons/Github'
import LinkedIn from 'components/PageElements/Icons/LinkedIn'
import DevTo from 'components/PageElements/Icons/DevTo'
import Mastodon from 'components/PageElements/Icons/Mastodon'
import {
  TopBar,
  HidingTitle,
  NavigationLinks,
} from 'components/PageElements/TopBar'
import { SimilarPosts } from 'components/PageElements/SimilarPosts/SimilarPosts'
import * as styles from 'components/PageElements/Blog/Blog.css'

const SyntaxHighlighter = dynamic(
  () =>
    import('../../components/PageElements/SyntaxHighligher/SyntaxHighlighter'),
  {
    ssr: false,
    loading: () => <div className={styles.codeBackground} />,
  }
)

const components = {
  h1: ({ children }: any) => <T.ContentSubTitle>{children}</T.ContentSubTitle>,
  h2: ({ children }: any) => (
    <T.ContentMinorTitle>{children}</T.ContentMinorTitle>
  ),
  h3: ({ children }: any) => (
    <T.ContentMinorTitle>{children}</T.ContentMinorTitle>
  ),
  h4: ({ children }: any) => (
    <T.ContentMinorTitle>{children}</T.ContentMinorTitle>
  ),
  h5: ({ children }: any) => (
    <T.ContentMinorTitle>{children}</T.ContentMinorTitle>
  ),
  code: ({ language, children }: any) => {
    return (
      <SyntaxHighlighter
        language={language}
        children={children}
        showLineNumbers
        customStyle={{
          fontFamily: 'Source Code Pro',
          borderRadius: '6px',
        }}
        wrapLines
      />
    )
  },
  p: ({ children }: any) => <T.Paragraph>{children}</T.Paragraph>,
  a: ({ href, children }: any) => <T.Link href={href}>{children}</T.Link>,
  li: ({ children }: any) => <T.ListItem>{children}</T.ListItem>,
  img: (props: any) => (
    <img className={styles.image} src={props.src} alt={props.alt} />
  ),
  blockquote: ({ children }: any) => (
    <blockquote className={styles.blockQuote}>{children}</blockquote>
  ),
  strong: ({ children }: any) => {
    return <T.Strong>{children}</T.Strong>
  },
}

const convertDate = (dateString: string) => {
  try {
    return new Date(dateString).toLocaleDateString(undefined, {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    })
  } catch (e) {
    return new Date(dateString).toISOString()
  }
}

const BlogPage = ({
  id,
  content,
  description,
  title,
  date,
  updatedDate,
  canotical,
  similarPosts,
}: any) => (
  <React.Fragment>
    <Head>
      <title>{title} - Aki Rautio</title>
      <meta property="og:title" content={title} />
      <meta property="og:type" content="article" />
      <meta property="og:site_name" content="Aki Rautio" />
      <meta property="og:url" content={`https://akirautio.com/blog/${id}`} />
      <meta
        property="og:image"
        content={`https://akirautio.com/assets/social/${id}.png`}
      />
      <meta
        property="article:published_time"
        content={new Date(date).toISOString().substring(0, 10)}
      />
      {updatedDate !== null ? (
        <meta
          property="article:modified_time"
          content={new Date(updatedDate).toISOString().substring(0, 10)}
        />
      ) : undefined}
      <meta property="article:author" content="https://akirautio.com" />
      {description !== null ? (
        <React.Fragment>
          <meta property="og:description" content={description} />
          <meta name="twitter:description" content={description} />
          <meta name="description" content={description} />
        </React.Fragment>
      ) : undefined}

      <meta name="twitter:title" content={`${title} - Aki Rautio`} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta
        name="twitter:image"
        content={`https://akirautio.com/assets/social/${id}.png`}
      />
      <link rel="me" href="https://masto.ai/@akirautio" />
    </Head>
    <TopBar>
      <HidingTitle>
        <Link href="/">
          <T.Logo style={{ fontSize: '0.8em' }}>Aki Rautio</T.Logo>
        </Link>
      </HidingTitle>
      <NavigationLinks>
        <Github /> <DevTo /> <Mastodon /> <LinkedIn />
      </NavigationLinks>
    </TopBar>
    <div className={styles.centerWidth}>
      <header className={styles.header}>
        <Link href="/">
          <T.Logo>Aki Rautio</T.Logo>
        </Link>
      </header>
      <div className={styles.content}>
        <div className={styles.contentTitle}>
          <T.ContentTitle>{title}</T.ContentTitle>
          <T.ContentDate suppressHydrationWarning={true}>
            {convertDate(date)}
          </T.ContentDate>
        </div>
        <ReactMarkdown components={components}>{content}</ReactMarkdown>
        <T.Link href={`${canotical}#comments`}>
          Comment or ask about the post in Dev.to
        </T.Link>
      </div>
      <SimilarPosts similar={similarPosts} />
      <footer className={styles.footer}>
        <span className={styles.footerLinks}>
          <Github /> <DevTo /> <Mastodon /> <LinkedIn />
        </span>
        <hr className={styles.footerLine} />
        <span className={styles.footerCopy}>
          Aki Rautio &copy; 2020 <br /> Logos by&nbsp;
          <T.Link
            href="https://www.freepik.com/flatart"
            target="_blank"
            rel="noopener"
          >
            Flatart
          </T.Link>
        </span>
      </footer>
    </div>
  </React.Fragment>
)

export async function getStaticPaths() {
  const paths = blogs.map((blog) => `/blog/${blog.id}`)
  return { paths, fallback: false }
}

interface BlogItem {
  id: string
  name: string
  description?: string
  publishDate: string
  updatedDate?: string
  canotical: string
  filename: string
  connected?: Array<string>
}

export async function getStaticProps({ params }: any) {
  try {
    const fs = require('fs')
    const blogItem: BlogItem | undefined = blogs.find(
      (blog) => blog.id === params.blogname
    )

    if (blogItem === undefined) {
      throw Error('No blog item found')
    }

    const data = fs
      .readFileSync(`./src/blogs/items/${blogItem.filename}`)
      .toString()

    return {
      props: {
        id: blogItem.id,
        title: blogItem.name,
        description: blogItem.description || null,
        date: blogItem.publishDate,
        updatedDate: blogItem?.updatedDate || null,
        content: data,
        canotical: blogItem.canotical,
        similarPosts:
          blogItem.connected?.map((key) => {
            const blog = blogs.find((blog) => blog.id === key)
            return { id: blog?.id, title: blog?.name }
          }) || [],
      },
    }
  } catch (e) {
    console.error(e)
  }
}

export default BlogPage
