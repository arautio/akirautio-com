import React from 'react'
import { Header } from '../components/Typography'
import { Container } from '../components/TextElements'

import Sun from '../components/PageElements/Sun'
import Waves from '../components/PageElements/Waves'

const NotFoundPage = () => (
  <React.Fragment>
    <Sun />
    <Container>
      <Header>404</Header>
    </Container>
    <Waves />
  </React.Fragment>
)

export default NotFoundPage
