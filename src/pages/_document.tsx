import React from 'react'
import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
} from 'next/document'
import HeadFile from '../config/head'

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    try {
      const initialProps = await Document.getInitialProps(ctx)
      return {
        ...initialProps,
      }
    } finally {
    }
  }

  render() {
    return (
      <Html lang="en">
        <Head>
          <meta charSet="UTF-8" />

          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&family=Poppins&family=Merriweather&display=swap"
            rel="stylesheet"
          />
          <meta name="twitter:site" content="@akirautio" />
          <link rel="me" href="https://masto.ai/@akirautio" />
          <HeadFile />
          <script
            defer
            src="https://static.cloudflareinsights.com/beacon.min.js"
            data-cf-beacon='{"token": "866827783ba74c8daae08f081bcd6c17"}'
          ></script>
        </Head>

        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
