import React from 'react'
import { AppProps } from 'next/app'
import Head from 'next/head'
import PlausibleProvider from 'next-plausible'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <PlausibleProvider domain="akirautio.com">
      <React.Fragment>
        <Head>
          <title>Aki Rautio</title>
        </Head>
        <Component {...pageProps} />
        <style jsx global>{``}</style>
      </React.Fragment>
    </PlausibleProvider>
  )
}
