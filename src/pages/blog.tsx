import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import blogs from '../blogs/blogs.json'

import * as T from '../components/Typography'
import Github from '../components/PageElements/Icons/Github'
import LinkedIn from '../components/PageElements/Icons/LinkedIn'
import DevTo from '../components/PageElements/Icons/DevTo'
import Mastodon from '../components/PageElements/Icons/Mastodon'
import {
  TopBar,
  HidingTitle,
  NavigationLinks,
} from 'components/PageElements/TopBar'
import * as styles from 'components/PageElements/Blog/Blog.css'

const convertDate = (dateString: string) => {
  try {
    return new Date(dateString).toLocaleDateString()
  } catch (e) {
    return new Date(dateString).toISOString()
  }
}

const BlogList = ({ blogsPosts, title, description }: any) => (
  <React.Fragment>
    <Head>
      <title>{title} - Aki Rautio</title>
      <meta property="og:title" content={title} />
      <meta property="og:type" content="website" />
      <meta property="og:site_name" content="Aki Rautio" />
      <meta property="og:url" content={`https://akirautio.com/blog`} />
      <meta
        property="og:image"
        content="https://akirautio.com/icons/android-chrome-256x256.png"
      />
      <meta property="article:author" content="https://akirautio.com" />
      {description !== null ? (
        <React.Fragment>
          <meta property="og:description" content={description} />
          <meta name="twitter:description" content={description} />
          <meta name="description" content={description} />
        </React.Fragment>
      ) : undefined}

      <meta name="twitter:title" content={`${title} - Aki Rautio`} />
      <link rel="me" href="https://masto.ai/@akirautio" />
    </Head>
    <TopBar>
      <HidingTitle>
        <Link href="/">
          <T.Logo style={{ fontSize: '0.8em' }}>Aki Rautio</T.Logo>
        </Link>
      </HidingTitle>
      <NavigationLinks>
        <Github /> <DevTo /> <Mastodon /> <LinkedIn />
      </NavigationLinks>
    </TopBar>
    <div className={styles.centerWidth}>
      <header className={styles.header}>
        <Link href="/">
          <T.Logo>Aki Rautio</T.Logo>
        </Link>
      </header>
      <div className={styles.content}>
        <T.ContentTitle>Latest posts</T.ContentTitle>
        <div className={styles.list}>
          {blogsPosts.map((blog: any) => (
            <div className={styles.listItem} key={blog.id}>
              <div className={styles.titleList}>
                <h3 className={styles.titleItem}>
                  <Link href={`/blog/${blog.id}`} passHref>
                    <T.Link>{blog.name}</T.Link>
                  </Link>
                </h3>
                <span className={styles.dateParagraph}>
                  {convertDate(blog.updateDate || blog.publishDate)}
                </span>
              </div>
              <T.ListParagraph>{blog.description}</T.ListParagraph>
            </div>
          ))}
        </div>
      </div>
      <footer className={styles.footer}>
        <span className={styles.footerLinks}>
          <Github /> <DevTo /> <Mastodon /> <LinkedIn />
        </span>
        <hr className={styles.footerLine} />
        <span className={styles.footerCopy}>
          Aki Rautio &copy; 2020 <br /> Logos by&nbsp;
          <T.Link
            href="https://www.freepik.com/flatart"
            target="_blank"
            rel="noopener"
          >
            Flatart
          </T.Link>
        </span>
      </footer>
    </div>
  </React.Fragment>
)

export async function getStaticProps({}: any) {
  return {
    props: {
      blogsPosts: blogs,
      title: 'Latest blog posts',
      description:
        "My thoughts about software development related to topics I'm working on.",
    },
  }
}

export default BlogList
