import React from 'react'
import Head from 'next/head'

import Sun from '../components/PageElements/Sun'
import Waves from '../components/PageElements/Waves'

import { Header, LinkText } from '../components/Typography'
import { Container, Links, LinkItem } from '../components/TextElements'
import links from '../config/links'

const IndexPage = () => (
  <React.Fragment>
    <Head>
      <meta property="og:title" content="Aki Rautio" />
      <meta property="og:type" content="website" />
      <meta property="og:site_name" content="Aki Rautio" />
      <meta property="og:url" content="https://akirautio.com/" />
      <meta
        property="og:image"
        content="https://akirautio.com/icons/android-chrome-256x256.png"
      />
      <link rel="canonical" href="https://akirautio.com" />
      <meta
        name="og:description"
        content="I'm Aki and I'm a software engineer building frontend solutions to enhance processes and user experience."
      />
      <meta
        name="twitter:description"
        content="I'm Aki and I'm a software engineer building frontend solutions to enhance processes and user experience."
      />
      <meta name="twitter:title" content="Aki Rautio" />
      <link rel="me" href="https://masto.ai/@akirautio" />
    </Head>
    <Sun />
    <Container>
      <React.Fragment>
        <Header>Aki Rautio</Header>
        <Links>
          <React.Fragment>
            <LinkItem>
              <LinkText href={links.linkedin} aria-label="My LinkedIn profile">
                LinkedIn
              </LinkText>
            </LinkItem>
            <LinkItem>
              <LinkText href={links.github} aria-label="My Github profile">
                Github
              </LinkText>
            </LinkItem>
            <LinkItem>
              <LinkText
                href={links.devto}
                aria-label="My DevTo profile for blog posts"
              >
                DevTo
              </LinkText>
            </LinkItem>
            <LinkItem>
              <LinkText
                href={links.mastodon}
                aria-label="My Mastodon for social networking"
              >
                Mastodon
              </LinkText>
            </LinkItem>
            <LinkItem>
              <LinkText href={links.blog} aria-label="My blog posts">
                Blog
              </LinkText>
            </LinkItem>
          </React.Fragment>
        </Links>
      </React.Fragment>
    </Container>
    <Waves />
  </React.Fragment>
)

export default IndexPage
