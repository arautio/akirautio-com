import type { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  // @TODO: Read request email address and name
  // @TODO: Call moosend to add user https://moosendapp.docs.apiary.io/#reference/subscribers/add-or-update-subscribers/adding-subscribers
  // @TOOD: If all good return status JOINED otherwise ERROR
  try {
    const request = JSON.parse(req.body)
    const response = await fetch(
      `https://api.moosend.com/v3/subscribers/${process.env.MAILING_LIST_ID}/subscribe.json?apikey=${process.env.MOOSEND_API_KEY}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          Email: request.email,
        }),
      }
    )
    if (!response.ok) {
      res.statusCode = response.status
      res.setHeader('Content-Type', 'application/json')
      res.end(JSON.stringify({ status: 'ERROR', error: response.statusText }))
    } else {
      const responseJSON = await response.json()

      if (responseJSON.Error !== null) {
        res.statusCode = response.status
        res.setHeader('Content-Type', 'application/json')
        res.end(JSON.stringify({ status: 'ERROR', error: responseJSON.Error }))
      } else {
        res.statusCode = 200
        res.setHeader('Content-Type', 'application/json')
        res.end(JSON.stringify({ status: 'JOINED' }))
      }
    }
  } catch (e) {
    console.log(e)
    res.statusCode = 500
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify({ status: 'ERROR', error: 'Error happened' }))
  }
}
